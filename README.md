## What this project does

This server project is a GraphQL API wrapper for the the freely available pokeapi. It is engineered after the requirements for my Browser Pokedex (https://gitlab.com/StringsOfArrays/pokedex-frontend). It is capable of the following things.

1) Requests all Pokemondata and stores them in a local cache to offer a rich set of data to the frontend, far more than the pokeapi itself would offer when trying to get all Pokemon at once and manouvering around the problematic scenario of making around 1000 requests at once.

2) The project ships with a pre-built cache that can be checked on its completion on every start of the project.

3) Possibility to request a specific pokemon's data which will then be fetched from the pokeapi and served via GraphQL

## Requirements and Setup

Make sure you are connected to the internet when using this project and be sure that the pokeapi is reachable. To do so, you can check the pokeapis status here: https://updown.io/akzp

To get the project started do the following steps:
1) **npm install** or **yarn install**, installs all the modules defined in the dependency section of the package.json

2) **npm start** or **yarn start** will run the project. On start, the project will ask you whether you would like to check the pokeapi for updates to keep the cache up to date. As of Octobre 2020, you can skip that step by entering **n**. If you want to check for updates anyway, be aware that the 
cache will only update when the amount of receivable items in the pokeapi has changed.

3) You can verify that the server is running, by visiting http://localhost:4000 and try the "hello" query in the GraphQL-Playground

**NOTE:** DO NOT delete pkmn.json! If you want to manually clear the cache, replace the content in this file with an empty object -> {}

## Technologies

GraphQL-Yoga, TypeScript