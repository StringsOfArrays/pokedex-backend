const fetch = require("node-fetch");
const axios = require("axios");
const fs = require("fs");
const cachedPokemon = require("./cache/pkmn.json");
const api = "https://pokeapi.co/api/v2";
const directory = "./cache/pkmn.json";
const io = require("console-read-write");

(async function getListFromApi() {
  console.log("Gotta cache em' all!");
  if (Object.keys(cachedPokemon).length === 0) {
    console.log("Your cache is empty! Lets build your cache first.");
    fetchFromApi();
    return;
  }

  let answer = "";
  while (answer.toLowerCase() !== ("y" || "n")) {
    console.log("Do you want to check the API for updates? (y/n)");
    answer = await io.read();
    if (answer.toLowerCase() === "n") return;
  }
  console.log("...checking your files...");
  fetchFromApi();
})();

async function fetchFromApi() {
  const count = await fetch(api + "/pokemon");
  const amount = JSON.parse(await count.text()).count;
  const cacheLength = Array.from(cachedPokemon).length;
  if (amount === cacheLength) {
    console.log("your cache is already up to date!");
    return;
  }
  const allPokemon = await fetch(api + `/pokemon?limit=${amount}`);
  const allPokemonArray = JSON.parse(await allPokemon.text()).results;

  const intervall = 50;
  const amountOfSubarrays = (amount - (amount % intervall)) / intervall;
  const arrayOfSubarrays = [];
  for (let index = 0; index < amountOfSubarrays; index++) {
    arrayOfSubarrays.push(allPokemonArray.splice(0, intervall));
  }
  if (allPokemonArray.length > 0) {
    arrayOfSubarrays.push(allPokemonArray);
  }

  let fetchedPokemon = [];
  let index = 0;
  let progress = 0;

  const fetchPortion = async () => {
    try {
      const fetchedSubPokemon = await Promise.all(
        arrayOfSubarrays[index].map(async (pokemon) => {
          const res = await axios(pokemon.url, {
            method: "GET",
            timeout: 10000,
          });
          return JSON.stringify({
            name: res.data.name,
            id: res.data.id,
            sprites: res.data.sprites,
            types: res.data.types,
          });
        })
      );
      fetchedPokemon = [...fetchedPokemon, ...fetchedSubPokemon];

      progress += arrayOfSubarrays[index].length;
      console.log(`${progress} of ${amount} pokemon loaded`);
      if (index === arrayOfSubarrays.length - 1) {
        await fs.writeFile(directory, "[" + fetchedPokemon + "]", () =>
          console.log("done")
        );
        return;
      }
      index++;
    } catch {
      console.log("retry");
    }
    fetchPortion();
  };

  await fetchPortion();
}
