import { ResolverMap } from "./types/graphql-utils";
import fetch from "node-fetch";
import * as pokeList from "../cache/pkmn.json";

const api = "https://pokeapi.co/api/v2";
export const resolvers: ResolverMap = {
  Query: {
    hello: (_: any, { name }: GQL.IHelloOnQueryArguments) => `Hello ${name}`,
    getPokemon: async (_: any, { input }: GQL.IGetPokemonOnQueryArguments) => {
      const metric = input?.id || input?.name;
      if (metric) {
        const basePokemon = await fetch(api + "/pokemon/" + metric + "/");
        const extendedData = await fetch(
          api + "/pokemon-species/" + metric + "/"
        );
        const extendedDataJson = await extendedData.json();
        const extension = {
          color: extendedDataJson.color?.name,
          evolves_from_species: extendedDataJson.evolves_from_species?.name,
          description: extendedDataJson.flavor_text_entries?.find(
            (enrty: any) => enrty.language.name === "en"
          )?.flavor_text,
        };
        const pokemon = await basePokemon.json();
        const baseData = {
          pokemon: {
            id: pokemon.id,
            name: pokemon.name,
            types: pokemon.types,
            sprites: pokemon.sprites,
          },
          height: pokemon.height,
          weight: pokemon.weight,
        };
        return { ...baseData, ...extension };
      }
      return null;
    },
    getAllPokemon: async () => {
      return pokeList;
    },
  },
};
