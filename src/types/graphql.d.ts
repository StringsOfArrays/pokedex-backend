// tslint:disable
// graphql typescript definitions

declare namespace GQL {
  interface IGraphQLResponseRoot {
    data?: IQuery;
    errors?: Array<IGraphQLResponseError>;
  }

  interface IGraphQLResponseError {
    /** Required for all errors */
    message: string;
    locations?: Array<IGraphQLResponseErrorLocation>;
    /** 7.2.2 says 'GraphQL servers may provide additional entries to error' */
    [propName: string]: any;
  }

  interface IGraphQLResponseErrorLocation {
    line: number;
    column: number;
  }

  interface IQuery {
    __typename: 'Query';
    hello: string;
    getPokemon: IExtendedPokemon;
    getAllPokemon: Array<IPokemon>;
  }

  interface IHelloOnQueryArguments {
    name?: string | null;
  }

  interface IGetPokemonOnQueryArguments {
    input?: IGetPokemonInput | null;
  }

  interface IGetPokemonInput {
    id?: number | null;
    name?: string | null;
  }

  interface IExtendedPokemon {
    __typename: 'ExtendedPokemon';
    pokemon: IPokemon;
    color: string | null;
    height: number | null;
    weight: number | null;
    evolves_from_species: string | null;
    description: string | null;
  }

  interface IPokemon {
    __typename: 'Pokemon';
    id: string;
    name: string;
    types: Array<ISlotAndListType | null> | null;
    sprites: ISprites | null;
  }

  interface ISlotAndListType {
    __typename: 'SlotAndListType';
    slot: number | null;
    type: IListType | null;
  }

  interface IListType {
    __typename: 'ListType';
    name: string;
    url: string;
  }

  interface ISprites {
    __typename: 'Sprites';
    back_default: string | null;
    back_female: string | null;
    back_shiny: string | null;
    back_shiny_female: string | null;
    front_default: string | null;
    front_female: string | null;
    front_shiny: string | null;
    front_shiny_female: string | null;
  }

  interface IListPokemon {
    __typename: 'ListPokemon';
    name: string;
    url: string;
  }
}

// tslint:enable
